This package is maintained with git-buildpackage(1). It follows DEP-14 for
branch naming (e.g. using debian/master for the current version in Debian
unstable due Debian Python team policy).

It uses pristine-tar(1) to store enough information in git to generate bit
identical tarballs when building the package without having downloaded an
upstream tarball first.

When working with patches it is recommended to use "gbp pq import" to import
the patches, modify the source and then use "gbp pq export --commit" to commit
the modifications.

The changelog is generated using "gbp dch" so if you submit any changes don't
bother to add changelog entries but rather provide a nice git commit message
that can then end up in the changelog.

It is recommended to build the package with pbuilder using:

    gbp buildpackage --git-pbuilder

For information on how to set up a pbuilder environment see the git-pbuilder(1)
manpage. In short:

    DIST=sid git-pbuilder create
    gbp clone https://salsa.debian.org/python-team/packages/pastescript.git
    cd pastescript
    gbp buildpackage --git-pbuilder


Notes the upstream tests
------------------------

Upstream stated that the pastscript project is feature complete and is being
maintained on life support.
Unfortunately the upstream test suite is still based on Nose. The current test
set is modified by patches and called with additional Pytest options so we can
at least run parts of the upstream tests. This will not work without problem
ins the future as also the devlopment of the Python language is moving forward
and some old code will probably not excecutale with newer Python releases in
the future.

If you can help to migrate the current tests to Pytest than this is highly
appreciated! Please get in contact with the Debian Python Team if you can
provide any patches that improve the situation.

 -- Carsten Schoenert <c.schoenert@t-online>  Sun, 20 Nov 2022 12:26:00 +0100
